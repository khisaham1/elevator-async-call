## API Structure
    Elevator_project
      urls_v1 -- api application folder
      postgres -- postgresql service folder
        Dockerfile
        beem_v1.sql
      Dockerfile
      docker-compose.yml   
      requirements.txt   

## Deployment Instructions
    # Ensure you have docker install on your deployment server

    clone the repo and open terminal inside the root of the repo
    execute `docker-compose up` command

## ENDPOINTS
1. http://127.0.0.1:8000/api/elevators/  -- CRUD for elevator (supply <int:pk>)
2. http://127.0.0.1:8000/api/elevator-action/  -- elevator action call (<int:pk>)
3. http://127.0.0.1:8000/api/elevator-action-logs/ -- visualize elevator logs
