from rest_framework import serializers
from .models import Elevator, ElevatorAction, ElevatorLog


class ElevatorActionSerializer(serializers.ModelSerializer):
    class Meta:
        model = ElevatorAction
        fields = "__all__"


class ElevatorSerializer(serializers.ModelSerializer):
    elevator_actions = ElevatorActionSerializer(many=True, read_only=True)

    class Meta:
        model = Elevator
        fields = "__all__"


class ElevatorLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = ElevatorLog
        fields = "__all__"
