from django.db import models
import asyncio
from django.http import JsonResponse


class Elevator(models.Model):
    name = models.CharField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    async def move(self, target_floor):
        while self.current_floor != target_floor:
            if self.current_floor < target_floor:
                self.current_floor += 1
            else:
                self.current_floor -= 1

            # Update elevator position in the database
            # await database_sync_to_async(self.save)()

            await asyncio.sleep(5)  # Elevator moves every 5 seconds

    async def open_doors(self):
        # Open doors logic
        await asyncio.sleep(2)

    async def close_doors(self):
        # Close doors logic
        await asyncio.sleep(2)

    async def simulate_movement(self, target_floor):
        await self.open_doors()
        await self.move(target_floor)
        await self.close_doors()

def elevator_simulation(target_floor, elevator_id):
    elevator = Elevator.objects.get(id=elevator_id)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(elevator.simulate_movement(target_floor))

# API view to trigger elevator movement simulation
def simulate_elevator_movement(request, elevator_id, target_floor):
    elevator_simulation(int(target_floor), int(elevator_id))
    return JsonResponse({'status': 'Simulation started'})

class ElevatorAction(models.Model):
    elevator = models.ForeignKey(Elevator, on_delete=models.CASCADE, null=False)
    current_floor = models.IntegerField(default=1)
    target_floor = models.IntegerField(default=1)
    direction = models.CharField(max_length=50, default="up")
    state = models.CharField(max_length=20, default="idle")

class ElevatorLog(models.Model):
    elevator = models.ForeignKey(Elevator, on_delete=models.CASCADE)
    event = models.CharField(max_length=200)
    timestamp = models.DateTimeField(auto_now_add=True)
