from rest_framework.test import APIClient
from django.test import TestCase
from .models import Elevator, ElevatorAction, ElevatorLog
from .serializer import (
    ElevatorSerializer,
    ElevatorActionSerializer,
    ElevatorLogSerializer,
)
from urls_v1.views import ElevatorViewSet, ElevatorActionViewSet, ElevatorInfo


class ElevatorViewSetTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.elevator_data = {"name": "Test Elevator"}

    def test_create_elevator(self):
        response = self.client.post(
            "/api/elevators/", self.elevator_data, format="json"
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Elevator.objects.count(), 1)
        self.assertEqual(Elevator.objects.get().name, "Test Elevator")

    def test_retrieve_elevator(self):
        elevator = Elevator.objects.create(name="Test Elevator")
        response = self.client.get(f"/api/elevators/{elevator.id}/")
        self.assertEqual(response.status_code, 200)
        elevator_data = response.data.get("elevator", {})
        self.assertEqual(elevator_data.get("name"), "Test Elevator")


class ElevatorActionViewSetTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.elevator = Elevator.objects.create(name="Test Elevator")
        self.elevator_action_data = {
            "elevator": self.elevator.id,
            "current_floor": 1,
            "target_floor": 5,
            "direction": "up",
            "state": "active",
        }

    def test_create_elevator_action(self):
        response = self.client.post(
            "/api/elevator-action/", self.elevator_action_data, format="json"
        )
        self.assertEqual(response.status_code, 201)
        self.assertEqual(ElevatorAction.objects.count(), 1)

class ElevatorLogViewSetTest(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.elevator = Elevator.objects.create(name="Test Elevator")
        self.elevator_action = ElevatorAction.objects.create(
            elevator=self.elevator,
            current_floor=1,
            target_floor=5,
            direction="up",
            state="active"
        )
        self.elevator_log_data = {
            "elevator": self.elevator.id,
            "event": "Elevator event log"
        }

    def test_create_elevator_log(self):
        response = self.client.post('/api/elevator-action-logs/', self.elevator_log_data, format='json')
        self.assertEqual(response.status_code, 201)
        self.assertEqual(ElevatorLog.objects.count(), 1)

# Run the tests
if __name__ == "__main__":
    import unittest

    unittest.main()
