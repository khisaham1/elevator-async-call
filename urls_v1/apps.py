from django.apps import AppConfig


class UrlsV1Config(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "urls_v1"
