import datetime
from urls_v1.serializer import (
    ElevatorLogSerializer,
    ElevatorSerializer,
    ElevatorActionSerializer,
)
from rest_framework import viewsets
from django.db import connection
from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Elevator, ElevatorAction, ElevatorLog


class ElevatorViewSet(viewsets.ModelViewSet):
    queryset = Elevator.objects.all()
    serializer_class = ElevatorSerializer

    def perform_create(self, serializer):
        elevator = serializer.save()
        self.create_elevator_log(elevator, f"Elevator {elevator.name} created.")

    def perform_update(self, serializer):
        elevator = serializer.save()
        self.create_elevator_log(elevator, f"Elevator {elevator.name} updated.")

    def perform_destroy(self, instance):
        elevator_name = instance.name
        instance.delete()
        self.create_elevator_log(instance, f"Elevator {elevator_name} deleted.")

    def create_elevator_log(self, elevator, event):
        ElevatorLog.objects.create(elevator=elevator, event=event)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()

        elevator_serializer = self.get_serializer(instance)
        elevator_actions_queryset = instance.elevatoraction_set.all()
        elevator_actions_serializer = ElevatorActionSerializer(
            elevator_actions_queryset, many=True
        )

        response_data = {
            "elevator": elevator_serializer.data,
            "elevator_actions": elevator_actions_serializer.data,
        }

        return Response(response_data)


class ElevatorActionViewSet(viewsets.ModelViewSet):
    queryset = ElevatorAction.objects.all()
    serializer_class = ElevatorActionSerializer

    def perform_create(self, serializer):
        elevator_action = serializer.save()
        self.create_elevator_log(
            elevator_action.elevator, f"Elevator {elevator_action.elevator.name} Current Floor {elevator_action.current_floor} Target Floor {elevator_action.target_floor} Action {elevator_action.direction}  State {elevator_action.state}"
        )

    def create_elevator_log(self, elevator, event):
        ElevatorLog.objects.create(elevator=elevator, event=event)
    # async def perform_create(self, serializer):
    #     elevator_action = serializer.save()
    #     await self.create_elevator_log(elevator_action)

    # async def create_elevator_log(self, elevator_action):
    #     event = f"Elevator {elevator_action.elevator.name} Current Floor {elevator_action.current_floor} Target Floor {elevator_action.target_floor} Action {elevator_action.state} State {elevator_action.state}"

    #     # Asynchronous database operation using the database package
    #     with connection.connection() as async_conn:
    #         async with async_conn.cursor() as cursor:
    #             await cursor.execute(
    #                 "INSERT INTO urls_v1_elevatorlog (elevator_id, event) VALUES (%s, %s)",
    #                 [elevator_action.elevator_id, event],
    #             )


class ElevatorLogViewSet(viewsets.ModelViewSet):
    queryset = ElevatorLog.objects.all()
    serializer_class = ElevatorLogSerializer


class ElevatorInfo(RetrieveAPIView):
    queryset = Elevator.objects.all()
    serializer_class = ElevatorSerializer
