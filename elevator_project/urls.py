"""
URL configuration for elevator_project project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from urls_v1.views import ElevatorActionViewSet, ElevatorInfo, ElevatorViewSet, ElevatorLogViewSet

router = routers.DefaultRouter()
router.register(r'elevators', ElevatorViewSet)
router.register(r'elevator-action', ElevatorActionViewSet)
router.register(r'elevator-action-logs', ElevatorLogViewSet)

urlpatterns = [
    path("admin/", admin.site.urls),
    path('api/', include(router.urls)),
    path('elevator_info/<int:pk>/', ElevatorInfo.as_view(), name='elevator_info'),
]